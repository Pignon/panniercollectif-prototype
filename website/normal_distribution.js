function boxMullerTransform() {
    const u1 = Math.random();
    const u2 = Math.random();
    
    const z0 = Math.sqrt(-2.0 * Math.log(u1)) * Math.cos(2.0 * Math.PI * u2);
    const z1 = Math.sqrt(-2.0 * Math.log(u1)) * Math.sin(2.0 * Math.PI * u2);
    
    return { z0, z1 };
}

function getNormallyDistributedRandomNumber(mean, stddev) {
    const { z0, _ } = boxMullerTransform();
    
    return z0 * stddev + mean;
}

function getCarConsumption() {
    car_fuel_mean = 10.6286 // km / liter
    car_fuel_standard_deviation = 2.12572
    return Math.max(0, 1/getNormallyDistributedRandomNumber(car_fuel_mean, car_fuel_standard_deviation));
}


function getTruckConsumption() {
    truck_fuel_mean = 2.850837942859442 //km / liter 
    truck_fuel_standard_deviation = 1
    return Math.max(0, 1/getNormallyDistributedRandomNumber(truck_fuel_mean, truck_fuel_standard_deviation));
}