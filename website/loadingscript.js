const apiKey = "AAPK20e7e25aa3b04ccb915212b829885568wOu6BIFXfkMog0Jf6XCSw2gVrULvJcsMZF5V8AgsChaXC-hCpXMx0DRUlNdlTpTJ";
const basemapEnum = "ArcGIS:Navigation";

center_caen = [-0.35912, 49.18585] // Caen


// Symbols
var car = {
    'type': 'FeatureCollection',
    'features': [
        {
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'Point',
                'coordinates': origin
            }
        }
    ]
}

const map = new maplibregl.Map({
    container: "map", // the id of the div element
    style: `https://basemaps-api.arcgis.com/arcgis/rest/services/styles/${basemapEnum}?type=style&token=${apiKey}`,
    zoom: 12, // starting zoom

    center: center_caen // Caen

});

function addRouteLayer() {

    map.addSource("car", {
        type: "geojson",
        data: {
            type: "FeatureCollection",
            features: []
        }
    });

    map.addLayer({
        id: "car",
        type: "line",
        source: "car",

        paint: {
            "line-color": "rgba(75, 255, 10, 1)",
            "line-width": 4,
            "line-opacity": 0.6
        }
    });

}

function updateRoute(coordinates = []) {
    map.getSource("car").setData({
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'MultiLineString',
                    'coordinates': coordinates
                }
            }
        ]
    });

    /*
    const authentication = arcgisRest.ApiKeyManager.fromKey(apiKey);
    arcgisRest
        .solveRoute({
        stops: [startCoords, endCoords],
        endpoint: "https://route-api.arcgis.com/arcgis/rest/services/World/Route/NAServer/Route_World/solve",
        authentication
        })
    
        .then((response) => {
        //route = response.routes.geoJson;
        map.getSource("route").setData(response.routes.geoJson);
        })
    
        .catch((error) => {
        console.error(error);
        alert("There was a problem using the route service. See the console for details.");
        });
        */

}

/*
map.on("click", (e) => {

const coordinates = e.lngLat.toArray();
const point = {
    type: "Point",
    coordinates
};

if (currentStep === "start") {
    console.log("Start point: " + coordinates)
    map.getSource("start").setData(point);
    startCoords = coordinates;
    const empty = {
    type: "FeatureCollection",
    features: []
    };
    map.getSource("end").setData(empty);
    map.getSource("route").setData(empty);
    endCoords = null;
    currentStep = "end";
} else {
    console.log("End point: " + coordinates)
    map.getSource("end").setData(point);
    endCoords = coordinates;
    currentStep = "start";
}

if (startCoords && endCoords) {
    updateRoute(startCoords, endCoords);
}

});
*/