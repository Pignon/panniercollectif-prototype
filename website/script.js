CAR_PROBABILITY = .8
TRUCK_PROBABILITY = 1 - CAR_PROBABILITY

class Citizen {
    constructor(){
        if (Math.random() <= CAR_PROBABILITY){ 
            this.vehicleType = "CAR"
            this.fuelConsumption = getCarConsumption();
        }else{
            this.fuelConsumption = getTruckConsumption();
        }
        this.startingLocation = getCitizenStartingLocation();
        this.path = getPath(this.startingLocation);
    }
}

async function startAnimation(population_size) {
    citizens = []
    for (let i=0; i<population_size; i++){
        citizens.push(new Citizen());
    }
    let iteration = 0;
    while (true) {
        await sleep(100);
        let coordinates = getCoordinates(citizens, iteration++);
        updateRoute(coordinates);
    }
}

function getCitizenStartingLocation() {
    // todo Luc
    return [center_caen[0] + Math.random()/100, center_caen[1] + Math.random()/100]
}

function getPath(startingLocation) {
    path = [startingLocation]
    // todo Thomas
    for (let i=0; i<1000; i++){
        path.push([path[i][0]+Math.random()/100, path[i][1]+Math.random()/100])
    }
    return path;
}

function distance_2(start, end) {
    return (start[0]-end[0]).pow(2) + (start[1]-end[1]).pow(2)
}

LINE_LENGTH = 5
function getCoordinates(citizens, iteration) {
    let coordinates = []
    for (citizen of citizens) {
        path = []
        for (let i=Math.max(0, iteration-LINE_LENGTH); i<=iteration; i++){
            if (i >= citizen.path.length) break;
            path.push(citizen.path[i]);
        }
        coordinates.push(path);
    }
    // last_pushed_coordinates = coordinates[0][coordinates[0].length-1]
    // coordinates[0].push([last_pushed_coordinates[0]+.001, last_pushed_coordinates[1]+.01])
    return coordinates;
}

start = [-0.3652628061701648, 49.18634595930911]

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


$(document).ready(init);

function init() {
    map.on("load", () => {    
        addRouteLayer();
    });
}

let currentStep = "start";
let startCoords, endCoords;
