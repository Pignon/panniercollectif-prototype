# PannierCollectif-Prototype

Final prototype of the ensicaen's intensive project carried out at the Dôme de caen. 
It allows you to visualize how much more efficient a collective delivery is in terms of energy sobriety compared to our current way of shopping.

## Installation

To install the server, be sure that you have installed [Node.js](https://nodejs.org/en/download/) run the commands:

    cd server
    npm i
    npm run start
To execute the server, inside the server folder, you only need to execute the command:

    npm run start

## Live Demo

http://intensif07.ensicaen.fr:8080/

## Explanation of the different parts of the project:

     - presentation_website: The configuration related to the redirection of the website created on Wordpress to the ENSICAEN VM via port 80
    
     - server: The configuration linked to the server of the simulation website via port 8080.

     - website: The code of the simulation opposing the journeys of citizens in supermarkets to the deliveries made by the government following the reform of the Collective Basket

## Description of simulation:

	We will take the city of Caen as an example during this simulation.
	It will be divided into neighborhoods, with different supermarkets available to citizens.

	We will then simulate the optimal route between housing and the supermarket for each citizen.

	In addition, calculations will be made in order to know the consumption made by car and by transport truck in order to be able to illustrate this through graphs.