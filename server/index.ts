import express from 'express'
import dotenv from 'dotenv'
import path from 'path';

dotenv.config({path: path.join(__dirname, "/.env")})

const app = express();
const port = process.env.PORT || 8000;

const WEBSITE_SOURCE_PATH : string = path.join(__dirname, '..','website')

//ROUTES
app.get('/', (req, res) => {
    res.set('Content-Type', 'text/html');
    res.sendFile(path.join(WEBSITE_SOURCE_PATH,'/trajectory.html'));

});

app.get('/style.css', function(req, res) {
    res.sendFile(path.join(WEBSITE_SOURCE_PATH,'style.css'));
  })

app.get('/script.js', function(req, res) {
    res.sendFile(path.join(WEBSITE_SOURCE_PATH,'script.js'));
})

app.get('/normal_distribution.js', function(req, res) {
    res.sendFile(path.join(WEBSITE_SOURCE_PATH,'normal_distribution.js'));
})

app.get('/loadingscript.js', function(req, res) {
    res.sendFile(path.join(WEBSITE_SOURCE_PATH,'loadingscript.js'));
})



// JSON DATA FILE
app.get('/data', (req, res) => {
    res.set('Content-Type', "application/json");
    res.sendFile(path.join(__dirname, '/data.json'))
})

app.listen(port, () => {
    console.log('Server app listening on port ' + port);
});