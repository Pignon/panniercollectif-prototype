import {MongoClient} from 'mongodb';
import * as dotenv from 'dotenv'

dotenv.config( { path: __dirname + '/.env' } );

const uri = process.env.URI as string;

export const client = new MongoClient(uri);