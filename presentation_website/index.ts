import express from 'express'
import dotenv from 'dotenv'
import path from 'path';

dotenv.config({path: path.join(__dirname, "/.env")})

const app = express();
const port = process.env.REDIRECTION_PORT || 443;

const WEBSITE_SOURCE_PATH : string = path.join(__dirname, '..','website')
const REDIRECTION_URL : string = process.env.REDIRECTION_URL as string

//ROUTES

app.get('/', (req, res) => {
    res.redirect(REDIRECTION_URL)
});

app.listen(port, () => {
    console.log(`webServer ready to redirect to ${REDIRECTION_URL} on port ${port}`);
});